import Vue from 'vue'
import Router from 'vue-router'
import FieldsShow from '../pages/FieldsShow'
import StatsShow from '../pages/StatsShow'

Vue.use(Router)

export default new Router({
  mode: 'history',

  routes: [
    {
      path: '/',
      name: 'fields',
      component: FieldsShow
    },

    {
      path: '/statistics',
      name: 'stats',
      component: StatsShow
    }
  ]
})
