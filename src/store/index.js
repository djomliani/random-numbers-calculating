import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersist from 'vuex-persist'
import { createResult, getTime } from '../js/result'

Vue.use(Vuex)

const vuexLocalStorage = new VuexPersist({
  key: 'vuex',
  storage: window.localeStorage
})

const fieldsNames = [...Array(10)].map((val, i) => String.fromCharCode(i + 65))
const fields = {}
const time = getTime()

const defaultData = () => {
  fieldsNames.forEach(field => {
    fields[field] = [[3, time]]
  })
  
  return fields
}

export default new Vuex.Store({
  state: {
    fields: defaultData ()
  },

  mutations: {
    UPDATE_FIELD (state, payload) {
      state.fields[payload.field].unshift(payload.change)
    },

    CLEAR_FIELDS (state) {
      state.fields = defaultData ()
    }
  },

  actions: {
    updateField ({ commit }, payload) {
      const change = createResult(payload)

      const field = {
        field: payload.field,
        change
      }

      commit('UPDATE_FIELD', field)
    },

    clearFields ({ commit }) {
      commit('CLEAR_FIELDS')
    }
  },

  plugins: [vuexLocalStorage.plugin]
})
