export const createResult = (payload) => {
  const number = randomNumber()
  const sign = randomSign()
  const time = getTime()

  const result = (payload.value + sign * number).toFixed(2)

  const change = [parseFloat(result), time]

  return change
}

const randomNumber = () => {
  return (Math.random() * 1 + 1).toFixed(2)
}

const randomSign = () => {
  return Math.random() >= 0.5 ? 1 : -1
}

export const getTime = () => {
  const date = new Date()

  const hours = date.getHours()
  const minutes = date.getMinutes()
  const seconds = date.getSeconds()

  const time = hours + ' : ' + minutes + ' : ' + seconds

  return time
}
